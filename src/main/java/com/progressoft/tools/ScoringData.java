package com.progressoft.tools;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ScoringData implements ScoringSummary {
    private final List<BigDecimal> columnData;
    private final List<BigDecimal> sortList = new LinkedList<>();

    public ScoringData(List<BigDecimal> columnData) {
        this.columnData = columnData;
        sortList.addAll(this.columnData);
        Collections.sort(sortList);
    }

    @Override
    public BigDecimal mean() {
        BigDecimal meanSum = BigDecimal.ZERO;
        for (BigDecimal element : columnData) {
            meanSum = meanSum.add(element);
        }
        return meanSum.divide(BigDecimal.valueOf(columnData.size()), RoundingMode.HALF_EVEN).setScale(2);
    }


    @Override
    public BigDecimal standardDeviation() {
        // standard = sqrt(variance)
        return BigDecimal.valueOf(
                        Math.sqrt(variance().doubleValue())
                )
                .setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal variance() {
        //variance = sum(Xi - mean)^2 / N
        BigDecimal varSum = BigDecimal.ZERO;
        for (BigDecimal element : columnData) {
            varSum = varSum.add((element.subtract(mean())).pow(2));
        }
        return varSum.divide(
                        new BigDecimal(columnData.size()), RoundingMode.HALF_EVEN
                )
                .setScale(0, RoundingMode.HALF_EVEN)
                .setScale(2);

    }

    @Override
    public BigDecimal median() {
        BigDecimal median;
        if (sortList.size() % 2 == 0) {
            median = sortList.get(sortList.size() / 2).
                    setScale(2, RoundingMode.HALF_EVEN);
        } else {
            median = sortList.get((sortList.size() - 1) / 2).add(sortList.get((sortList.size() + 1) / 2))
                    .divide(new BigDecimal("2"), RoundingMode.HALF_EVEN)
                    .setScale(2);
        }
        return median;
    }

    @Override
    public BigDecimal min() {
        return sortList.get(0).setScale(2, RoundingMode.HALF_EVEN);
    }

    @Override
    public BigDecimal max() {
        return sortList.get(sortList.size() - 1).setScale(2, RoundingMode.HALF_EVEN);

    }

}
