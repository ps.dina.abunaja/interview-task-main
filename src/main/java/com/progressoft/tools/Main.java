package com.progressoft.tools;

import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        DataNormalizer dataNormalizer = new DataNormalizer();
        if (args[3].equals("min-max"))
            dataNormalizer.minMaxScaling(Paths.get(args[0]), Paths.get(args[1]), args[2]);
        else
            dataNormalizer.zscore(Paths.get(args[0]), Paths.get(args[1]), args[2]);
    }
}
