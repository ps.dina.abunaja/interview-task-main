package com.progressoft.tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DataReader {

    public DataReaderInfo readingData(Path filePath, String columnName) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath.toFile()))) {
            Map<String, List<String>> fullData = new LinkedHashMap<>();
            DataReaderInfo dataReaderInfo = new DataReaderInfo();
            AddHeader(dataReaderInfo, columnName, br, fullData);

            String data;
            dataReaderInfo.setArrayOfData(null);
            while ((data = br.readLine()) != null) {
                addLineOfData(dataReaderInfo, fullData, dataReaderInfo.getArrayOfHeader(), data);
            }
            dataReaderInfo.setFullData(fullData);
            dataReaderInfo.setColumnToNormalize(parsingData(fullData, columnName));
            return dataReaderInfo;

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("source file not found");

        } catch (IOException ioException) {
            throw new IllegalArgumentException("column " + columnName + " not found");
        }
    }

    private void addLineOfData(DataReaderInfo dataReaderInfo, Map<String, List<String>> fullData, List<String> arrayOfHeader, String data) {
        String[] dataArray = data.split(",");
        for (int i = 0; i < dataArray.length; i++) {
            fullData.get(arrayOfHeader.get(i)).add(dataArray[i]);
        }
        if (dataReaderInfo.getArrayOfData() != null) {
            dataReaderInfo.setArrayOfData(String.format("%s \n%s", dataReaderInfo.getArrayOfData(), data));
        } else {
            dataReaderInfo.setArrayOfData(data);
        }
    }

    private void AddHeader(DataReaderInfo dataReaderInfo, String columnName, BufferedReader br, Map<String, List<String>> fullData) throws IOException {
        List<String> arrayOfHeader = List.of(br.readLine().split(","));
        dataReaderInfo.setIndexOfColName(arrayOfHeader.indexOf(columnName));
        for (String h : arrayOfHeader) {
            fullData.put(h, new ArrayList<>());
        }
        dataReaderInfo.setArrayOfHeader(arrayOfHeader);
    }

    public List<BigDecimal> parsingData(Map<String, List<String>> dataReader, String columnName) {
        List<BigDecimal> bigDecimalList = new LinkedList<>();
        if (dataReader.containsKey(columnName)) {
            for (String value : dataReader.get(columnName)) {
                bigDecimalList.add(new BigDecimal(value));
            }
        } else {
            throw new IllegalArgumentException("column " + columnName + " not found");
        }
        return bigDecimalList;

    }
}
