package com.progressoft.tools;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

public class DataNormalizer implements Normalizer {
    public static final String Z_SCALE = "z";
    public static final String MIN_MAX_SCALE = "mm";
    private final DataReader dataReader = new DataReader();
    private final CSVFileModifier csvFileModifier = new CSVFileModifier();

    @Override
    //z_norm = (s - mean) / sigma
    public ScoringSummary zscore(Path csvPath, Path destPath, String colToStandardize) {
        DataReaderInfo dataReaderInfo = dataReader.readingData(csvPath, colToStandardize);
        ScoringData dataScoring = new ScoringData(dataReaderInfo.getColumnToNormalize());
        List<String> scaleData = new LinkedList<>();
        for (BigDecimal element : dataReaderInfo.getColumnToNormalize()) {
            scaleData.add(String.valueOf(((element.subtract(dataScoring.mean()))
                    .divide(dataScoring.standardDeviation(), RoundingMode.HALF_EVEN)
                    .setScale(2))));
        }
        csvFileModifier.addNewColumnToCSVFile(dataReaderInfo,
                destPath,
                colToStandardize,
                scaleData,
                Z_SCALE);
        return dataScoring;

    }

    @Override
    public ScoringSummary minMaxScaling(Path csvPath, Path destPath, String colToNormalize) {
        // X_scaled = (X - min())/(max()-min())
        DataReaderInfo dataReaderInfo = dataReader.readingData(csvPath, colToNormalize);
        ScoringData dataScoring = new ScoringData(dataReaderInfo.getColumnToNormalize());
        BigDecimal maxSubMin = dataScoring.max().subtract(dataScoring.min());
        List<String> scaleData = new LinkedList<>();
        for (BigDecimal element : dataReaderInfo.getColumnToNormalize()) {
            scaleData.add(String.valueOf((element.subtract(dataScoring.min())
                    .divide(maxSubMin, RoundingMode.HALF_EVEN)
                    .setScale(2))));
        }
        csvFileModifier.addNewColumnToCSVFile(
                dataReaderInfo,
                destPath,
                colToNormalize,
                scaleData,
                MIN_MAX_SCALE);

        return dataScoring;
    }


}
