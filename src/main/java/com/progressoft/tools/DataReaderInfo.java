package com.progressoft.tools;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public final class DataReaderInfo {
    private Map<String, List<String>> fullData;
    private List<BigDecimal> columnToNormalize;
    private List<String> arrayOfHeader;
    private int indexOfColName;
    private String arrayOfData;

    public Map<String, List<String>> getFullData() {
        return fullData;
    }

    public void setFullData(Map<String, List<String>> fullData) {
        this.fullData = fullData;
    }

    public List<BigDecimal> getColumnToNormalize() {
        return columnToNormalize;
    }

    public void setColumnToNormalize(List<BigDecimal> columnToNormalize) {
        this.columnToNormalize = columnToNormalize;
    }

    public List<String> getArrayOfHeader() {
        return arrayOfHeader;
    }

    public void setArrayOfHeader(List<String> arrayOfHeader) {
        this.arrayOfHeader = arrayOfHeader;
    }

    public int getIndexOfColName() {
        return indexOfColName;
    }

    public void setIndexOfColName(int indexOfColName) {
        this.indexOfColName = indexOfColName;
    }

    public String getArrayOfData() {
        return arrayOfData;
    }

    public void setArrayOfData(String arrayOfData) {
        this.arrayOfData = arrayOfData;
    }
}
