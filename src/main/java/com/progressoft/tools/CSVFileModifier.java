package com.progressoft.tools;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CSVFileModifier {

    public void addNewColumnToCSVFile(DataReaderInfo dataReaderInfo, Path destPath, String colName, List<String> data, String type) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(destPath.toFile()))) {
            addFieldToHeader(dataReaderInfo.getArrayOfHeader(), colName, dataReaderInfo.getIndexOfColName(), type, bw);
            addDataToCSVFile(dataReaderInfo.getFullData(), dataReaderInfo.getIndexOfColName(), data, bw);
        } catch (IOException ioException) {
            throw new IllegalArgumentException("column " + colName + " not found");
        }
    }

    private void addDataToCSVFile(Map<String, List<String>> dataReader, int indexOfColName, List<String> data, BufferedWriter bw) throws IOException {
        List<List<String>> tempArray = new LinkedList<>();
        dataReader.forEach((k, v) -> tempArray.add(v));
        tempArray.add(indexOfColName + 1, data);
        for (List<String> strings : transpose(tempArray)) {
            String s1 = String.join(",", strings);
            bw.write(s1);
            bw.newLine();
        }
    }

    private void addFieldToHeader(List<String> arrayOfHeader, String colName, int indexOfColName, String type, BufferedWriter bw) throws IOException {
        String newColName = colName + "_" + type;
        List<String> header = new LinkedList<>(arrayOfHeader);
        header.add(indexOfColName + 1, newColName);
        String s = String.join(",", header);
        bw.write(s);
        bw.newLine();
    }

    private List<List<String>> transpose(List<List<String>> list) {
        final int N = list.stream().mapToInt(List::size).max().orElse(-1);
        List<Iterator<String>> iterList = list.stream().map(List::iterator).collect(Collectors.toList());
        return IntStream.range(0, N)
                .mapToObj(n -> iterList.stream()
                        .filter(Iterator::hasNext)
                        .map(Iterator::next)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }
}
